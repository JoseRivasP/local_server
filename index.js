const readline = require('readline');
const { existsSync } = require('fs');
const express = require('express');
const app = express();
const path = require('path');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log("\x1b[34m", "Welcome please type the path of the file to use.");
console.log("\x1b[34m", "(Type ctrl + c to leave the app)");



const process_file = () => {
    rl.question('With wich folder would you like to work:\n', (data) => {

        // TODO: Log the answer in a database
        console.log("\x1b[34m", "--------------------Processing...----------------------------");

        if (existsSync('../' + data)) {
            local_server_deploy('../' + data);
        } else {
            console.log("\x1b[31m", "This directory doesn't exist");
            return process_file();
        }
    });
};

const local_server_deploy = (pathFile) => {
    rl.question('With wich port do you want to deploy the local server: ', (port) => {
        rl.question('With wich file would you like to work:\n', (file) => {
            if (existsSync(pathFile + "/" + file)) {
                app.use((req, res, next) => {
                    res.header('Access-Control-Allow-Origin', '*', 'https://dev.niehs.nih.gov/',
                        'https://tst.niehs.nih.gov/', 'https://stage.niehs.nih.gov/',
                        'https://www.niehs.nih.gov/', 'https://tools.niehs.nih.gov',
                        'https://grants.nih.gov', 'https://jobs.nih.gov', 'https://seek.niehs.nih.gov',
                        'https://kids.niehs.nih.gov', 'https://factor.niehs.nih.gov', 'https://ewps.niehs.nih.gov',
                        'https://webtrends-sdc.niehs.nih.gov');
                    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
                    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
                    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
                    next();
                });

                app.use('/', express.static(pathFile, { redirect: true }));

                app.get('/', (req, res, next) => {
                    const options = {
                        root: path.resolve(pathFile),
                        dotfiles: 'deny',
                        headers: {
                            'x-timestamp': Date.now(),
                            'x-sent': true
                        }
                    }

                    res.sendFile(file, options, function (err) {
                        if (err) {
                            next(err)
                        } else {
                            console.log('Sent:', file)
                        }
                    })
                });

                app.listen(port, () => {
                    console.log("\x1b[32m", `Your app is deployed in http://localhost:${port}`);
                    console.log("\x1b[32m", `For restart the app please type rs and Enter`);
                    console.log("\x1b[32m", "(Type ctrl + c to leave the app)");
                });
            } else {
                console.log("\x1b[31m", "This file doesn't exist. Please enter the port and file name again.");
                return local_server_deploy(pathFile);
            }
        });
    });
};

process_file();
